<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('jne', function () {
    $array = array(12,43,66,21,56,43,43,78,78,100,43,43,43,21);
    $vals = array_count_values($array);
    echo 'No. of NON Duplicate Items: '.count($vals).'<br><br>';
    dd($vals);
});
// [1, 2, 3, 4, 5, 6, 7, 8, 9]

Route::get('/', function () {
	if (Auth::check()) {
		return redirect('dashboard');
	}
    return view('auth.login');
});

Route::get('moving-average', 'MovingAverageController@index');

Route::get('apriori', 'AprioriController@index');
Route::post('apriori', 'AprioriController@prosesApriori');
Route::get('process_log', 'AprioriController@process_log');
Route::get('hasil/{id}', 'AprioriController@hasil');

Route::get('kodepos', function (\App\DataTables\KodeposDataTable $dataTable) {
    return $dataTable->render('kodepos');
});

Route::get('ongkir/jx', function (\App\DataTables\JXOngkirDataTable $dataTable) {
	return $dataTable->render('jx');
});
Route::get('ongkir/jne', function () {
    $provinsi     = \App\Library\RajaOngkirLib::provinsi();
    return view('jne',compact('provinsi'));
});
Route::get('rate/jx', function () {

    $ongkir = \App\JXOngkir::all();
    foreach ($ongkir as $key => $value) {
    	$date['data'] = [
    		'items' => $value->distrisct,
    	];
    }

    return response()->json($data);
    
});

Route::get('cache',function(){
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
});

Route::post('cek_kota', 'AjaxController@cekkota');
Route::post('cek_kecamatan', 'AjaxController@cekkecamatan');
Route::post('cekongkir', 'AjaxController@cekongkir');
Route::post('cek_sub', 'AjaxController@ceksub');

Auth::routes(['register' => false]);
Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');

Route::resource('order', 'OrderController');
Route::get('order/approve/{id}', 'OrderController@approve');
Route::get('order/retur/{id}', 'OrderController@retur');
Route::resource('penjualan', 'PenjualanController');
Route::get('kas/import-dana-shopee', 'KasController@import_dana_shopee')->name('kas.import-dana-shopee');
Route::post('kas/import-dana-shopee', 'KasController@post_import_dana_shopee')->name('kas.import-dana-shopee-post');
Route::resource('kas', 'KasController');
Route::resource('produk', 'ProdukController');