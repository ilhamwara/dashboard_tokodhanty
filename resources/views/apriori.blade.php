@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{asset('daterangepicker.css')}}">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('include.alert')
            <div class="card">
                <div class="card-header">
                    <h3>Proses Apriori</h3>
                </div>
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <form action="{{url('apriori')}}" enctype="multipart/form-data" method="POST">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="">Tanggal</label>
                                    <input type="text" class="form-control tanggal" name="tanggal" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Min Support</label>
                                    <input type="number" value="0" class="form-control" name="min_support" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Min Confidence</label>
                                    <input type="number" value="0" class="form-control" name="min_confidence" required>
                                </div>
                                <div class="form-group">
                                    <a href="{{url('process_log')}}" class="btn btn-success">Lihat Process Log</a>
                                    <button class="btn btn-primary">Hitung</button>
                                </div>
                            </form>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
<script src="{{asset('daterangepicker.js')}}"></script>
<script>
$(function() {
  $('.tanggal').daterangepicker({
    opens: 'left'
  }, function(start, end, label) {
  });
});
</script>
@endsection
