@extends('layouts.app')
@section('css')
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('include.alert')
            <div class="card">
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <form action="{{route('order.store')}}" enctype="multipart/form-data" method="POST">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="">Upload File Excel</label>
                                    <input type="file" name="file" class="form-control" required style="padding: 3px;">
                                </div>
                                <div class="form-group">
                                    <small><i>Untuk melakukan import file excel harus mengikuti sample excel disini, download file excel <a href="{{asset('template_order.xlsx')}}">disini</a></i></small>
                                </div>
                                <div class="form-group">
                                    <a href="{{route('order.index')}}" class="btn btn-success">Kembali ke Data Order</a>
                                    <button class="btn btn-primary">Import Data</button>
                                </div>
                            </form>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@endsection
