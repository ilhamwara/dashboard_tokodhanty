@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection
@section('content')
<div class="container">
    @if(!empty(request()->date))
        <div class="row mb-2">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h5><b>Total Order COD</h5>
                        <small><i><b>{{$tanggal_dari}} - {{$tanggal_sampai}}</b></i></b></small>
                        <br><br>
                        <h2>{{$order->where('type','COD')->count()}}</h2>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h5><b>Total Order Transfer</h5>
                        <small><i><b>{{$tanggal_dari}} - {{$tanggal_sampai}}</b></i></b></small>
                        <br><br>
                        <h2>{{$order->where('type','Transfer')->count()}}</h2>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-md-12">
                            <ul class="nav nav-pills">
                              <li class="nav-item">
                                <a class="nav-link {{(empty(request()->status) ? 'active' : '')}}" href="{{url('order')}}">Semua Pesanan</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link {{(!empty(request()->status) && (request()->status == 'Selesai') ? 'active' : '' )}}" href="{{url('order?status=Selesai')}}">Selesai</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link {{(!empty(request()->status) && (request()->status == 'Dikirim') ? 'active' : '' )}}" href="{{url('order?status=Dikirim')}}">Perlu Dikirim ( {{\App\Order::where('status','LIKE','%Dikirim%')->count()}} )</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link {{(!empty(request()->status) && (request()->status == 'Batal') ? 'active' : '' )}}" href="{{url('order?status=Batal')}}">Retur</a>
                              </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row mb-5">
                        <div class="col-md-12">
                            <form action="" method="GET" class="form-inline">
                                <div class="form-group mr-2">
                                    <input type="hidden" name="status" value="{{request()->status}}" class="form-control">
                                    <input type="text" required name="date" class="form-control date" required>
                                </div>
                                <div class="form-group mr-2">
                                    <select required name="type" id="" class="form-control">
                                        <option value="COD">COD</option>
                                        <option value="Transfer">Transfer</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary">Cari</button>
                                </div>
                            </form>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                {!! $dataTable->table(['width' => '100%', 'class' => 'table table-striped table-bordered']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
{!! $dataTable->scripts() !!}
<script>
    $('.date').daterangepicker({
        locale: {
            format: 'YYYY/MM/DD'
        }
    });
</script>
@endsection
