@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mb-2">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body bg-primary text-white">
                    <h5><b>Total Order</b> <br><small><i>Bulan ini</i></small></h5>
                    <h2>{{$order->count()}}</h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body bg-primary text-white">
                    <h5><b>Order COD</b> <br><small><i>Bulan ini</i></small></h5>
                    <h2>{{$cod->count()}}</h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body bg-primary text-white">
                    <h5><b>Order Transfer</b> <br><small><i>Bulan ini</i></small></h5>
                    <h2>{{$transfer->count()}}</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-2">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body bg-success text-white">
                    <h5><b>Total Order</b> <br><small><i>Hari ini</i></small></h5>
                    <h2>{{$today_order->count()}}</h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body bg-success text-white">
                    <h5><b>Order COD</b> <br><small><i>Hari ini</i></small></h5>
                    <h2>{{$today_cod->count()}}</h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body bg-success text-white">
                    <h5><b>Order Transfer</b> <br><small><i>Hari ini</i></small></h5>
                    <h2>{{$today_transfer->count()}}</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5><b>OMSET</b> <br><small><i>Bulan ini</i></small></h5>
                    <h2>Rp {{str_replace(',','.',number_format($kas))}}</h2>
                </div>
            </div>
        </div>
        {{-- <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h5><b>Penjualan COD</b> <br><small><i>Bulan ini</i></small></h5>
                    <h2>Rp {{str_replace(',','.',number_format($order_cod->sum('harga_jual')))}}</h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h5><b>Penjualan Transfer</b> <br><small><i>Bulan ini</i></small></h5>
                    <h2>Rp {{str_replace(',','.',number_format($order_transfer->sum('harga_jual')))}}</h2>
                </div>
            </div>
        </div> --}}
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="chart"></div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
    Highcharts.chart('chart', {

    title: {
        text: 'Grafik Penjualan 2021'
    },
    yAxis: {
        title: {
            text: 'Total'
        }
    },

    xAxis: {
        accessibility: {
            rangeDescription: ''
        }
    },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 1
        }
    },

    series: [{
        name: 'COD',
        data: [
            {{\App\Order::where('type','cod')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',1)->count()}},
            {{\App\Order::where('type','cod')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',2)->count()}},
            {{\App\Order::where('type','cod')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',3)->count()}},
            {{\App\Order::where('type','cod')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',4)->count()}},
            {{\App\Order::where('type','cod')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',5)->count()}},
            {{\App\Order::where('type','cod')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',6)->count()}},
            {{\App\Order::where('type','cod')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',7)->count()}},
            {{\App\Order::where('type','cod')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',8)->count()}},
            {{\App\Order::where('type','cod')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',9)->count()}},
            {{\App\Order::where('type','cod')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',10)->count()}},
            {{\App\Order::where('type','cod')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',11)->count()}},
            {{\App\Order::where('type','cod')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',12)->count()}}
        ]
    }, {
        name: 'Transfer',
        data: [
            {{\App\Order::where('type','transfer')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',1)->count()}},
            {{\App\Order::where('type','transfer')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',2)->count()}},
            {{\App\Order::where('type','transfer')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',3)->count()}},
            {{\App\Order::where('type','transfer')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',4)->count()}},
            {{\App\Order::where('type','transfer')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',5)->count()}},
            {{\App\Order::where('type','transfer')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',6)->count()}},
            {{\App\Order::where('type','transfer')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',7)->count()}},
            {{\App\Order::where('type','transfer')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',8)->count()}},
            {{\App\Order::where('type','transfer')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',9)->count()}},
            {{\App\Order::where('type','transfer')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',10)->count()}},
            {{\App\Order::where('type','transfer')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',11)->count()}},
            {{\App\Order::where('type','transfer')->groupBy('resi')->whereYear('tanggal','2020')->whereMonth('tanggal',12)->count()}}
        ]
    },{
        name: 'COD Retur',
        data: [
            {{\App\Order::where('status','Retur')->groupBy('resi')->where('type','cod')->whereYear('tanggal','2020')->whereMonth('tanggal',1)->count()}},
            {{\App\Order::where('status','Retur')->groupBy('resi')->where('type','cod')->whereYear('tanggal','2020')->whereMonth('tanggal',2)->count()}},
            {{\App\Order::where('status','Retur')->groupBy('resi')->where('type','cod')->whereYear('tanggal','2020')->whereMonth('tanggal',3)->count()}},
            {{\App\Order::where('status','Retur')->groupBy('resi')->where('type','cod')->whereYear('tanggal','2020')->whereMonth('tanggal',4)->count()}},
            {{\App\Order::where('status','Retur')->groupBy('resi')->where('type','cod')->whereYear('tanggal','2020')->whereMonth('tanggal',5)->count()}},
            {{\App\Order::where('status','Retur')->groupBy('resi')->where('type','cod')->whereYear('tanggal','2020')->whereMonth('tanggal',6)->count()}},
            {{\App\Order::where('status','Retur')->groupBy('resi')->where('type','cod')->whereYear('tanggal','2020')->whereMonth('tanggal',7)->count()}},
            {{\App\Order::where('status','Retur')->groupBy('resi')->where('type','cod')->whereYear('tanggal','2020')->whereMonth('tanggal',8)->count()}},
            {{\App\Order::where('status','Retur')->groupBy('resi')->where('type','cod')->whereYear('tanggal','2020')->whereMonth('tanggal',9)->count()}},
            {{\App\Order::where('status','Retur')->groupBy('resi')->where('type','cod')->whereYear('tanggal','2020')->whereMonth('tanggal',10)->count()}},
            {{\App\Order::where('status','Retur')->groupBy('resi')->where('type','cod')->whereYear('tanggal','2020')->whereMonth('tanggal',11)->count()}},
            {{\App\Order::where('status','Retur')->groupBy('resi')->where('type','cod')->whereYear('tanggal','2020')->whereMonth('tanggal',12)->count()}}
        ]
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
</script>
@endsection
