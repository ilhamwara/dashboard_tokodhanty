<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2-container .select2-selection--single {height: 40px!important;}
        .select2-container--default .select2-selection--single .select2-selection__rendered{line-height: 38px!important;}
        .select2-container--default .select2-selection--single{border: 1px solid #ddd!important;}
    </style>
</head>
<body>
    <div id="app">
        <main class="py-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="nav nav-tabs">
                                          <li class="nav-item">
                                            <a class="nav-link" href="{{url('ongkir/jx')}}">Ongkir JX</a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link active" href="{{url('ongkir/jne')}}">Ongkir JNE</a>
                                          </li>
                                        </ul>
                                        <br>
                                    </div>
                                    <div class="col-md-12">
                                        <form>
                                            <div class="form-group">
                                                <label>Provinsi</label>
                                                <select name="province" id="provinsi" class="form-control select2" id="">
                                                    @forelse($provinsi->rajaongkir->results as $dat_prov)
                                                        <option value="{{$dat_prov->province}}" data-val="{{$dat_prov->province_id}}">{{$dat_prov->province}}</option>
                                                    @endforeach
                                                </select>
                                              </div>
                                              <div class="form-group">
                                                <label>Kabpuaten / Kota</label>
                                                <select class="form-control select2 kotabaru" name="city" id="kota" title="City">
                                                    <option value="">-- Pilih --</option>
                                                </select>
                                              </div>
                                              <div class="form-group">
                                                <label>Kecamatan</label>
                                                <select class="form-control select2 kecamatan" name="sub_district" id="kec">
                                                    <option value="">-- Pilih --</option>
                                                </select>
                                              </div>
                                              <div class="ongkir"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
        $('#provinsi').change(function(){          
              var provinsinya = $(this).find(':selected').attr('data-val');
              $.ajax({
                type : 'POST',
                url  : '{{url('cek_kota')}}',
                data : {
                  provinsi : $(this).find(':selected').attr('data-val'),
                  _token : '{{csrf_token()}}'
                },  
                beforeSend: function() {
                    $('.coba').remove();
                    $('.ongkirbaru').remove();
                },
                success: function(response){
                  if($.isArray(response.data.post)){
                    $.each(response.data.post, function(key, value){
                        if (value.type == 'Kota') {
                            var pender = '<option class="coba" data-val="'+value.city_id+'" value="'+value.city_name+'">'+value.city_name+'</option>';
                            $('#kota').append(pender);
                        }
                        var input = '<input type="hidden" class="coba" name="id_provinsi" value="'+provinsinya+'">';
                        $('#provinsi').append(input);
                    });
                  }
                }
              });
          });
          $('#kota').change(function(){
            var citynya = $(this).find(':selected').attr('data-val');
            $.ajax({
              type : 'POST',
              url  : '{{url('cek_kecamatan')}}',
              data : {
                id_city : $(this).find(':selected').attr('data-val'),
                _token  : '{{csrf_token()}}'
              },  
              beforeSend: function() {
                  $('.coba_kec').remove();
                  $('.ongkirbaru').remove();
              },
              success: function(response){
                if($.isArray(response.data.post)){
                  $.each(response.data.post, function(key, value){
                      var pender = '<option class="coba_kec" data-val="'+value.subdistrict_id+'" value="'+value.subdistrict_name+'">'+value.subdistrict_name+'</option>';
                      $('#kec').append(pender);
                      var input = '<input type="hidden" class="coba_kec" name="id_city" value="'+citynya+'">'+
                                  '<input type="hidden" class="coba_kec" name="id_kec" value="'+value.subdistrict_id+'">';
                      $('#kota').append(input);
                  });
                }
              }
            });
        });
        $('.kecamatan').change(function(){
            $.ajax({
              type : 'POST',
              url  : '{{url('cekongkir')}}',
              data : {
                id_city  : $('#kota').find(':selected').attr('data-val'),
                provinsi : $('#provinsi').find(':selected').attr('data-val'),
                subdistrict : $('.kecamatan').find(':selected').attr('data-val'),
                _token  : '{{csrf_token()}}'
              },  
              beforeSend: function() {
                  $('.ongkirbaru').remove();
              },
              success: function(response){
                if($.isArray(response.data.post)){
                  $.each(response.data.post, function(key, value){
                      var input = '<div class="ongkirbaru">Service '+value.service+' Rp '+value.val+'<br></div>';
                      $('.ongkir').append(input);
                  });
                }
              }
            });
        })
    </script>
</body>
</html>