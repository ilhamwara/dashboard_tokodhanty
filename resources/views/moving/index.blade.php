@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{asset('daterangepicker.css')}}">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('include.alert')
            <div class="card">
                <div class="card-header">
                    <h3>Moving Average</h3>
                </div>
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <form action="" enctype="multipart/form-data" method="GET">
                                <div class="form-group">
                                    <label for="">Tahun</label>
                                    <select name="tahun" disabled class="form-control">
                                        <option value="2021">2021</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Periode</label>
                                    <select name="periode" class="form-control">
                                        <option {{(request()->periode == '1' ? 'selected' : '')}} value="1">Januari</option>
                                        <option {{(request()->periode == '2' ? 'selected' : '')}} value="2">Februari</option>
                                        <option {{(request()->periode == '3' ? 'selected' : '')}} value="3">Maret</option>
                                        <option {{(request()->periode == '4' ? 'selected' : '')}} value="4">April</option>
                                        <option {{(request()->periode == '5' ? 'selected' : '')}} value="5">Mei</option>
                                        <option {{(request()->periode == '6' ? 'selected' : '')}} value="6">Juni</option>
                                        <option {{(request()->periode == '7' ? 'selected' : '')}} value="7">July</option>
                                        <option {{(request()->periode == '8' ? 'selected' : '')}} value="8">Agustus</option>
                                        <option {{(request()->periode == '9' ? 'selected' : '')}} value="9">September</option>
                                        <option {{(request()->periode == '10' ? 'selected' : '')}} value="10">Oktober</option>
                                        <option {{(request()->periode == '11' ? 'selected' : '')}} value="11">November</option>
                                        <option {{(request()->periode == '12' ? 'selected' : '')}} value="12">Desember</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Produk</label>
                                    <select name="produk" class="form-control">
                                        @foreach($pro as $data)
                                        <option {{(request()->produk == $data->id ? 'selected' : '')}} value="{{$data->id}}">{{$data->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary">Hitung</button>
                                </div>
                            </form>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(!empty(request()->all()))
        <div class="col-md-12 mt-2">
            <div class="card">
                <div class="card-header">
                    <h3>Hasil</h3>
                </div>
                <div class="card-body">
                    <p>
                        <b>MA = ΣX / Jumlah Periode </b><br><br>
                        Keterangan : <br>
                        <b>MA</b> = Moving Average <br>
                        <b>ΣX</b>  = Keseluruhan Penjumlahan dari semua data periode waktu yang diperhitungkan <br>
                        <b>Jumlah Periode</b> = Jumlah Periode Rata-rata bergerak
                        <br><br>
                        <b>Perhitungan 4 Minggu Terakhir</b>
                    </p>
                    <table class="table table-bordered">
                        <tr>
                            <th class="text-center">No</th>
                            <th>Periode</th>
                            <th class="text-center">Produk</th>
                            <th class="text-center">Penjualan Pcs</th>
                        </tr>
                        @foreach($minggu as $k => $data)
                        <tr>
                            <td class="text-center">{{$k+1}}</td>
                            <td>Minggu Ke - {{$k+1}} ( {{date('d-m-Y',strtotime($data['from']))}} - {{date('d-m-Y',strtotime($data['to']))}} )</td>
                            <td class="text-center">{{\App\Produk::where('id',request()->produk)->value('nama')}}</td>
                            <td class="text-center">{{\App\Order::whereMonth('tanggal',request()->periode)->whereBetween('tanggal',[$data['from'],$data['to']])->sum('jumlah')}}</td>
                        </tr>
                        @endforeach
                        <tr class="bg-warning">
                            <td class="text-center">{{count($minggu)+1}}</td>
                            <td>Minggu Ke - {{count($minggu)+1}}</td>
                            <td class="text-center">{{\App\Produk::where('id',request()->produk)->value('nama')}}</td>
                            <td class="text-center">?</td>
                        </tr>
                    </table>
                    <p>
                        <b>Penyelesaiannya</b> : <br><br>

                        Perkiraan Penjualan adalah : <br>

                        <b>ΣX</b> = {{\App\Order::whereMonth('tanggal',request()->periode)->whereBetween('tanggal',[$minggu[0]['from'],$minggu[count($minggu)-1]['to']])->sum('jumlah')}} / {{count($minggu)}}<br>
                        <b>Moving Average</b> = {{\App\Order::whereMonth('tanggal',request()->periode)->whereBetween('tanggal',[$minggu[0]['from'],$minggu[count($minggu)-1]['to']])->sum('jumlah') / count($minggu)}} pcs <br>
                    </p>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
<script src="{{asset('daterangepicker.js')}}"></script>
<script>
$(function() {
  $('.tanggal').daterangepicker({
    opens: 'left'
  }, function(start, end, label) {
  });
});
</script>
@endsection
