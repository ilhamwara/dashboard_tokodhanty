@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection
@section('content')
<div class="container">
    @if(!empty(request()->date))
    <div class="row mb-2">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h5><b>Total Pemasukan</h5>
                    <small><i><b>{{$tanggal_dari}} - {{$tanggal_sampai}}</b></i></b></small>
                    <br><br>
                    <h2>Rp {{str_replace(',','.',number_format($kas->sum('pemasukan')))}}</h2>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h5><b>Total Pengeluaran</h5>
                    <small><i><b>{{$tanggal_dari}} - {{$tanggal_sampai}}</b></i></b></small>
                    <br><br>
                    <h2>Rp {{str_replace(',','.',number_format($kas->sum('pengeluaran')))}}</h2>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <form action="" method="GET" class="form-inline">
                                <div class="form-group mr-2">
                                    <input type="text" name="date" class="form-control date" required>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary">Cari</button>
                                </div>
                            </form>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            {!! $dataTable->table(['width' => '100%', 'class' => 'table table-striped table-bordered']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
{!! $dataTable->scripts() !!}
<script>
    $('.date').daterangepicker({
        locale: {
            format: 'YYYY/MM/DD'
        }
    });
</script>
@endsection
