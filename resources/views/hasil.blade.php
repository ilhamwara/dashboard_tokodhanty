@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('include.alert')
            <div class="card mb-3">
                <div class="card-header">
                    <h5><b>Rule ID : {{$RuleID->id}} </b></h5>
                </div>
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="col-sm-5">
                                <h5>Min Support: {{$RuleID->min_support}} </h5>
                                <h5>Min Confidence: {{$RuleID->min_confidence}} </h5>
                            </div>
                            <div class="col-sm-5">
                                <h5>Start Date: {{$RuleID->start_date}} </h5>
                                <h5>End Date: {{$RuleID->end_date}} </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <?php $data_confidence = []; ?>
            <div class="card mb-3">
                <div class="card-header">
                    <h5><b>Perhitungan Itemset 1</b></h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php $ItemSet1Lolos = []; ?>
                            <table class="myTable display table table-striped table-bordered"style="width:100%">
                                <thead>
                                    <tr>
                                        <th> No. </th>
                                        <th> Item 1 </th>
                                        <th> Jumlah </th>
                                        <th> Support </th>
                                        {{-- <th> Keterangan </th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($ItemSet1 as $j => $ItemSet1)
                                    <tr>
                                        <td align="center" width="5">{{$j+1}}</td>
                                        <td> {{$ItemSet1->atribut}}</td>
                                        <td> {{$ItemSet1->jumlah}}</td>
                                        <td> {{$ItemSet1->support}}</td>
                                        {{-- <td> {{($ItemSet1->lolos ==1 ? "Lolos":"Tidak Lolos")}}</td> --}}
                                    </tr>
                                        @if($ItemSet1->lolos == 1)
                                           <?php $ItemSet1Lolos[] = $ItemSet1; ?>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">
                    <h5><b>Perhitungan Itemset 2</b></h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php $ItemSet2Lolos = []; ?>
                            <table class="myTable display table table-striped table-bordered"style="width:100%">
                                <thead>
                                    <tr>
                                        <th> No. </th>
                                        <th> Item 1 </th>
                                        <th> Item 2 </th>
                                        <th> Jumlah </th>
                                        <th> Support </th>
                                        {{-- <th> Keterangan </th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($ItemSet2 as $j => $ItemSet2)
                                    <tr>
                                        <td align="center" width="5">{{$j+1 }}</td>
                                        <td> {{$ItemSet2->atribut1 }}</td>
                                        <td> {{$ItemSet2->atribut2 }}</td>
                                        <td> {{$ItemSet2->jumlah }}</td>
                                        <td> {{$ItemSet2->support }}</td>
                                        {{-- <td> {{($ItemSet2->lolos == 1 ? "Lolos" : "Tidak Lolos")}}</td> --}}
                                    </tr>
                                        @if($ItemSet2->lolos == 1)
                                           <?php $ItemSet2Lolos[] = $ItemSet2; ?>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">
                    <h5><b>Perhitungan Itemset 3</b></h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php $ItemSet3Lolos = []; ?>
                            <table class="myTable display table table-striped table-bordered"style="width:100%">
                                <thead>
                                    <tr>
                                        <th> No. </th>
                                        <th> Item 1 </th>
                                        <th> Item 2 </th>
                                        <th> Item 3 </th>
                                        <th> Jumlah </th>
                                        <th> Support </th>
                                        {{-- <th> Keterangan </th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($ItemSet3 as $j => $ItemSet3)
                                    <tr>
                                        <td align="center" width="5">{{$j+1 }}</td>
                                        <td> {{$ItemSet3->atribut1 }}</td>
                                        <td> {{$ItemSet3->atribut2 }}</td>
                                        <td> {{$ItemSet3->atribut3 }}</td>
                                        <td> {{$ItemSet3->jumlah }}</td>
                                        <td> {{$ItemSet3->support }}</td>
                                        {{-- <td> {{($ItemSet3->lolos==1?"Lolos":"Tidak Lolos") }}</td> --}}
                                    </tr>
                                        @if($ItemSet3->lolos == 1)
                                            <?php $ItemSet3Lolos[] = $ItemSet3; ?>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">
                    <h5><b>Confidence dari itemset 2 </b></h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="myTable display table table-striped table-bordered"style="width:100%">
                                <thead>
                                    <tr>
                                        <th> No. </th>
                                        <th> X => Y </th>
                                        <th> Support X U Y </th>
                                        <th> Support X </th>
                                        <th> Confidence </th>
                                        {{-- <th> Keterangan </th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($ConfidenceItemset2 as $j => $ConfidenceItemset2)
                                    <tr>
                                        <td align="center" width="5">{{ $j+1 }}</td>
                                        <td align="center" width="130">{{ $ConfidenceItemset2->kombinasi1." => ".$ConfidenceItemset2->kombinasi2 }}</td>
                                        <td align="center" width="130">{{ $ConfidenceItemset2->support_xUy }}</td>
                                        <td align="center" width="130">{{ $ConfidenceItemset2->support_x }}</td>
                                        <td align="center" width="130">{{ $ConfidenceItemset2->confidence }}</td>
                                        {{-- <td align="center" width="130">{{($ConfidenceItemset2->confidence <= $ConfidenceItemset2->min_confidence ?"Tidak Lolos":"Lolos") }}</td> --}}
                                    </tr>
                                        @if($ConfidenceItemset2->lolos == 1)
                                            <?php $data_confidence[] = $ConfidenceItemset2; ?>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">
                    <h5><b>Confidence dari itemset 3 </b></h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="displaymyTable table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th> No. </th>
                                        <th> X => Y </th>
                                        <th> Support X U Y </th>
                                        <th> Support X </th>
                                        <th> Confidence </th>
                                        {{-- <th> Keterangan </th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($ConfidenceItemset3 as $k => $ConfidenceItemset3)
                                    <tr>
                                        <td align="center">{{$k+1}}</td>
                                        <td align="center">{{$ConfidenceItemset3->kombinasi1." => ".$ConfidenceItemset3->kombinasi2 }}</td>
                                        <td align="center">{{$ConfidenceItemset3->support_xUy }}</td>
                                        <td align="center">{{$ConfidenceItemset3->support_x }}</td>
                                        <td align="center">{{$ConfidenceItemset3->confidence }}</td>
                                        {{-- <td align="center">{{($ConfidenceItemset3->confidence <= $ConfidenceItemset3->min_confidence ? "Tidak Lolos" : "Lolos")}}</td> --}}
                                    </tr>
                                        @if($ConfidenceItemset3->lolos == 1)
                                            <?php $data_confidence[] = $ConfidenceItemset3; ?>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">
                    <h5><b>Hasil Analisa</b></h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="myTable display table table-striped table-bordered"style="width:100%">
                                <thead>
                                    <tr>
                                        <th> No. </th>
                                        <th> Rule </th>
                                        <th> Confidence </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data_confidence as $j => $val)
                                    <tr>
                                        <td align="center" width="5">{{ $j+1 }}</td>
                                        <td> Jika konsumen membeli {{ $val->kombinasi1 }}, maka konsumen juga akan membeli {{ $val->kombinasi2 }}
                                        </td>
                                        <td align="center" width="150">{{$val->confidence}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready( function () {
        $('.myTable').DataTable();
    });
</script>
@endsection