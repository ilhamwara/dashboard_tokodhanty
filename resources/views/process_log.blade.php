@extends('layouts.app')
@section('css')
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('include.alert')
            <div class="card">
                <div class="card-header">
                    <h2>List Process</h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="display table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th> No. </th>
                                        <th> Start Date </th>
                                        <th> End Date </th>
                                        <th> Min Support </th>
                                        <th> Min Confidence </th>
                                        <th> Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($process_log as $k => $data)
                                    <tr>
                                        <td align="center">{{$k+1}}</td>
                                        <td align="center">{{$data->start_date }}</td>
                                        <td align="center">{{$data->end_date }}</td>
                                        <td align="center">{{$data->min_support }}</td>
                                        <td align="center">{{$data->min_confidence }}</td>
                                        <td align="center"><a href="{{url('hasil/'.$data->id) }}">View Hasil</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@endsection
