@extends('layouts.app')
@section('css')

@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('include.alert')
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{route('produk.store')}}" method="POST">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <input type="text" class="form-control" name="kode_produk" placeholder="Masukan Kode Produk" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="nama" placeholder="Masukan Nama Produk" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="harga_beli" placeholder="Masukan Harga Beli" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="harga_jual" placeholder="Masukan Harga Jual" required>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@endsection
