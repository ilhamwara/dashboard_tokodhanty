<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('produk_id');
            $table->string('nomer_pesanan')->nullable();
            $table->date('tanggal');
            $table->string('resi')->nullable();
            $table->string('nama');
            $table->string('hp');
            $table->string('alamat');
            $table->string('kurir');
            $table->string('spesifikasi');
            $table->string('type');
            $table->integer('harga_jual');
            $table->integer('ongkir')->nullable();
            $table->integer('diskon')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
