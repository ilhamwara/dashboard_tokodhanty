<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
        		'name' 					=> 'Administrator',
	            'username' 				=> 'admin',
	            'email' 				=> 'admin@tokodhanty.com',
	            'email_verified_at' 	=> date('Y-m-d'),
	            'password' 				=> Hash::make('password'),
	            'type' 					=> 'admin',
        	],
        	[
        		'name' 					=> 'Ilham',
	            'username' 				=> 'ilhamwara',
	            'email' 				=> 'ilhamwara@tokodhanty.com',
	            'email_verified_at' 	=> date('Y-m-d'),
	            'password' 				=> Hash::make('password'),
	            'type' 					=> 'cs',
        	],
        	[
        		'name' 					=> 'Mulia',
	            'username' 				=> 'mulia',
	            'email' 				=> 'mulia@tokodhanty.com',
	            'email_verified_at' 	=> date('Y-m-d'),
	            'password' 				=> Hash::make('password'),
	            'type' 					=> 'cs',
        	],
        	[
        		'name' 					=> 'Budi',
	            'username' 				=> 'budi',
	            'email' 				=> 'budi@tokodhanty.com',
	            'email_verified_at' 	=> date('Y-m-d'),
	            'password' 				=> Hash::make('password'),
	            'type' 					=> 'cs',
        	],
        	[
        		'name' 					=> 'Desi',
	            'username' 				=> 'desi',
	            'email' 				=> 'desi@tokodhanty.com',
	            'email_verified_at' 	=> date('Y-m-d'),
	            'password' 				=> Hash::make('password'),
	            'type' 					=> 'cs',
        	],
        ];

        $insert = User::insert($data);
    }
}
