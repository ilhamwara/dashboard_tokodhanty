<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produk';

    protected $fillable = [
    	'nama',
    	'kode_produk',
    	'harga_jual',
    	'harga_beli',
    ];
}
