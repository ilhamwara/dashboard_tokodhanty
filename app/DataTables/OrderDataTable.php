<?php

namespace App\DataTables;

use App\Order;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;
use URL;
use Auth;

class OrderDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)->editColumn('action',function($model){
                if (Auth::check()) {
                    if (($model->status == 'Sedang Dikirim') || ($model->status == 'Perlu Dikirim')) {
                        return '<a class="btn btn-block btn-sm btn-primary" target="_blank" href="https://berdu.id/cek-resi?courier=j%26t&code='.$model->resi.'&secret=1F76hc"> Tracking Resi</a> <a class="btn btn-block btn-sm btn-success" href="'.URL::to("order/approve/").'/'.$model->id.'"> Ubah Pesanan Selesai</a> <a class="btn btn-block btn-sm btn-danger " href="'.URL::to("order/retur/").'/'.$model->id.'"> Ubah Pesanan Retur</a>';
                    }else{
                        return '<a class="btn btn-block btn-sm btn-primary" target="_blank" href="https://berdu.id/cek-resi?courier=j%26t&code='.$model->resi.'&secret=1F76hc"> Tracking Resi</a>';
                    }
                }else{
                    return '<a class="btn btn-block btn-sm btn-primary" target="_blank" href="https://berdu.id/cek-resi?courier=j%26t&code='.$model->resi.'&secret=1F76hc"> Tracking Resi</a>';
                }
                
            })->rawColumns(['action']);
            // ->addColumn('action', 'order.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\App\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        if (($this->status == NULL) && ($this->tanggal_dari == 'kosong')) {
            return $model->with(['user','produk'])->orderBy('tanggal','DESC')->newQuery();
        }elseif($this->status == NULL){
            return $model->with(['user','produk'])->whereBetween('tanggal',[$this->tanggal_dari,$this->tanggal_sampai])->where('type',$this->type)->orderBy('tanggal','DESC')->newQuery();
        }elseif($this->status != NULL){
            return $model->with(['user','produk'])->where('status','LIKE','%'.$this->status.'%')->where('type',$this->type)->orderBy('tanggal','DESC')->newQuery();
        }else{
            return $model->with(['user','produk'])->where('status','LIKE','%'.$this->status.'%')->where('type',$this->type)->whereBetween('tanggal',[$this->tanggal_dari,$this->tanggal_sampai])->orderBy('tanggal','DESC')->newQuery();
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('order-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax();
                    // ->dom('Bfrtip')
                    // ->orderBy(1)
                    // ->buttons(
                    //     Button::make('create'),
                    //     Button::make('export'),
                    //     Button::make('print'),
                    //     Button::make('reset'),
                    //     Button::make('reload')
                    // );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // Column::computed('action')->title('Actions')->width(50)->render(function(){
            //     return 'function(data, type, row){
            //         // return "<a class=\'btn btn-block btn-sm btn-primary \' href=\''.URL::to("order/").'/"+row.id+"/edit\'> Edit <a class=\'btn btn-block btn-sm text-white btn-danger remove_data\' href=\'#\' data-href=\''.URL::to("order/").'/"+row.id+"/delete\'> Hapus</a>";
            //         return "<a target=\'_blank\' href=\'https://berdu.id/cek-resi?courier=j%26t&code="+row.resi+"&secret=1F76hc\' class=\'btn btn-block btn-sm btn-primary\'>Tracking Paket</a>";
                    
            //     }';
            // }),
            Column::computed('action')->title('Action'),
            // Column::make('id')->title('No')->render(function() {
            //     return 'function(data,type,fullData,meta){
            //         return meta.settings._iDisplayStart+meta.row+1;}';
            // })->width(10),
            Column::make('status')->title('Status Pesanan'),
            Column::make('nomer_pesanan')->title('No Pesanan'),
            Column::make('resi')->title('Resi'),
            Column::make('tanggal')->title('Tanggal'),
            Column::make('nama')->title('Nama'),
            Column::make('type')->title('Pengiriman'),
            // Column::make('alamat')->title('Alamat'),
            // Column::make('kurir')->title('Kurir'),
            // Column::make('produk.nama')->title('Produk'),
            // Column::make('harga_jual')->title('Nominal'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Order_' . date('YmdHis');
    }
}
