<?php

namespace App\DataTables;

use App\JXOngkir;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;

class JXOngkirDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', 'jxongkir.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\App\JXOngkir $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(JXOngkir $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('jxongkir-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax();
                    // ->dom('Bfrtip')
                    // ->orderBy(1)
                    // ->buttons(
                    //     Button::make('create'),
                    //     Button::make('export'),
                    //     Button::make('print'),
                    //     Button::make('reset'),
                    //     Button::make('reload')
                    // );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // Column::make('id')->title('No')->render(function() {
            //     return 'function(data,type,fullData,meta){
            //         return meta.settings._iDisplayStart+meta.row+1;}';
            // })->width(10),
            Column::computed('price')->title('Harga'),
            Column::make('district')->title('Kecamatan'),
            Column::computed('city')->title('Kota/Kab'),
            Column::computed('province')->title('Provinsi'),
            Column::computed('cod_support')->title('COD'),
            Column::computed('time')->title('Estimasi'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'JXOngkir_' . date('YmdHis');
    }
}
