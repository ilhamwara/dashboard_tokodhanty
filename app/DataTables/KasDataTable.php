<?php

namespace App\DataTables;

use App\Kas;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;
use URL;

class KasDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query);
            // ->addColumn('action', 'kas.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\App\Ka $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Kas $model)
    {
        // dd($this->tanggal_sampai);
        if ($this->tanggal_dari == 'kosong') {
            return $model->newQuery();
        }else{
            return $model->whereBetween('tanggal',[$this->tanggal_dari,$this->tanggal_sampai])->newQuery();
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('kas-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax();
                    // ->dom('Bfrtip')
                    // ->orderBy(1)
                    // ->buttons(
                    //     Button::make('create'),
                    //     Button::make('export'),
                    //     Button::make('print'),
                    //     Button::make('reset'),
                    //     Button::make('reload')
                    // );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // Column::computed('action')->title('Actions')->width(50)->render(function(){
            //     return 'function(data, type, row){
            //         return "<a class=\'btn btn-block btn-sm btn-info \' href=\''.URL::to("kas/").'/"+row.id+"/edit\'> <i class=\'fa fa-pencil\'></i></a> <a class=\'btn btn-block btn-sm text-white btn-danger remove_data\' href=\'#\' data-href=\''.URL::to("kas/").'/"+row.id+"/delete\'> <i class=\'fa fa-trash-o\'></i></a>";
            //     }';
            // }),
            Column::make('id')->title('No')->render(function() {
                return 'function(data,type,fullData,meta){
                    return meta.settings._iDisplayStart+meta.row+1;}';
            })->width(10),
            Column::make('tanggal')->title('Tanggal'),
            Column::make('keterangan')->title('Keterangan'),
            Column::make('pemasukan')->title('Pemasukan'),
            Column::make('pengeluaran')->title('Pengeluaran'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Kas_' . date('YmdHis');
    }
}
