<?php

namespace App\DataTables;

// use App\App\KodeposDataTable;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;
use App\Kodepos;

class KodeposDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', 'kodeposdatatable.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\App\KodeposDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Kodepos $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('kodeposdatatable-table')
                    ->columns($this->getColumns());
                    // ->minifiedAjax()
                    // ->dom('Bfrtip')
                    // ->orderBy(1)
                    // ->buttons(
                    //     Button::make('create'),
                    //     Button::make('export'),
                    //     Button::make('print'),
                    //     Button::make('reset'),
                    //     Button::make('reload')
                    // );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('subdistrict')->title('Kelurahan'),
            Column::make('district')->title('Kecamatan'),
            Column::computed('city')->title('Kota/Kab'),
            Column::computed('province')->title('Provinsi'),
            Column::make('kodepos')->title('Kodepos'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Kodepos_' . date('YmdHis');
    }
}
