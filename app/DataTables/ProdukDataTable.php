<?php

namespace App\DataTables;

use App\Produk;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;

class ProdukDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', 'produk.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\App\Produk $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Produk $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('produk-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax();
                    // ->dom('Bfrtip')
                    // ->orderBy(1)
                    // ->buttons(
                    //     Button::make('create'),
                    //     Button::make('export'),
                    //     Button::make('print'),
                    //     Button::make('reset'),
                    //     Button::make('reload')
                    // );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title('No')->render(function() {
                return 'function(data,type,fullData,meta){
                    return meta.settings._iDisplayStart+meta.row+1;}';
            })->width(10),
            Column::make('kode_produk')->title('Kode Produk'),
            Column::make('nama')->title('Nama Produk'),
            Column::make('harga_beli')->title('Harga Beli'),
            Column::make('harga_jual')->title('Harga Jual'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Produk_' . date('YmdHis');
    }
}
