<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcessLog extends Model
{
    protected $table = 'process_log';
}
