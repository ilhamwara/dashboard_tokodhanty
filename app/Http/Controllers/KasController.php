<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\KasDataTable;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\Kas as KasImport;
use App\Imports\DanaShopee;
use App\Kas;

class KasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(KasDataTable $dataTable)
    {
        if (empty(request()->date)) {
            return $dataTable->with(['tanggal_dari' => 'kosong'])->render('kas.index');
        }else{
            $date = explode(' - ', request()->date);
            $tanggal_dari   = $date[0];
            $tanggal_sampai = $date[1];

            $kas = Kas::whereBetween('tanggal',[$tanggal_dari,$tanggal_sampai])->get();

            return $dataTable->with(['tanggal_dari' => $tanggal_dari, 'tanggal_sampai' => $tanggal_sampai])->render('kas.index',compact('kas','tanggal_dari','tanggal_sampai'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $import  = Excel::import(new KasImport(), $request->file);
            
            return redirect()->back()->with('alert-success', 'Succes Import Data!');
        } catch (Exception $e) {
            return redirect()->back()->with('alert-danger', 'Gagal');
            
        }
    }

    public function import_dana_shopee()
    {
        return view('kas.import-shopee');
    }

    public function post_import_dana_shopee(Request $request)
    {
        try {
            $import  = Excel::import(new DanaShopee(), $request->file);
            
            return redirect()->back()->with('alert-success', 'Succes Import Data!');
        } catch (Exception $e) {
            return redirect()->back()->with('alert-danger', 'Gagal');
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
