<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\RajaOngkirLib;

class AjaxController extends Controller
{
    public function cekkota()
    {
        $kota = RajaOngkirLib::kota(request()->provinsi);
        if($kota){
            foreach ($kota->rajaongkir->results as $k => $v) {
                $data['post'][$k]['city_name']  = $v->city_name;
                $data['post'][$k]['city_id']    = $v->city_id;
                $data['post'][$k]['type']       = $v->type;
            }
            return response()->json(['status' => true, 'data' => $data, 'message' => ''], 200);
        }
    }
    public function cekkecamatan()
    {
        $sub    = RajaOngkirLib::subdistrict(request()->id_city);
        if($sub){
            foreach ($sub->rajaongkir->results as $k => $v) {
                $data['post'][$k]['subdistrict_name']  = $v->subdistrict_name;
                $data['post'][$k]['subdistrict_id']    = $v->subdistrict_id;
            }
        }
        return response()->json(['status' => true, 'data' => $data, 'message' => ''], 200);
    }
    public function cekongkir()
    {
        $cost    = RajaOngkirLib::cost(2119,request()->subdistrict,1000,'jne');
        if($cost){
            foreach ($cost->rajaongkir->results[0]->costs as $k => $v) {
                $data['post'][$k]['service']        = $v->service;
                $data['post'][$k]['description']    = $v->description;
                $data['post'][$k]['price']          = str_replace(',','.',number_format($v->cost[0]->value));
                $data['post'][$k]['val']          	= $v->cost[0]->value;
            }
        }
        return response()->json(['status' => true, 'data' => $data, 'message' => ''], 200);
    }
}
