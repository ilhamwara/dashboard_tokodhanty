<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Kas;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        $cod        = Order::where('type','cod')->whereYear('tanggal','2021')->whereMonth('tanggal','12')->get();
        $transfer   = Order::where('type','transfer')->whereYear('tanggal','2021')->whereMonth('tanggal','12')->get();
        $order      = Order::whereYear('tanggal','2021')->whereMonth('tanggal','12')->get();

        $today_cod        = Order::where('type','cod')->where('tanggal',date('Y-m-d'))->get();
        $today_transfer   = Order::where('type','transfer')->where('tanggal',date('Y-m-d'))->get();
        $today_order      = Order::where('tanggal',date('Y-m-d'))->get();

        $order_cod        = Order::where('type','cod')->whereYear('tanggal','2021')->whereMonth('tanggal','12')->where('status','selesai')->get();
        $order_transfer   = Order::where('type','transfer')->whereYear('tanggal','2021')->whereMonth('tanggal','12')->get();
        $pemasukan        = Kas::whereYear('tanggal','2021')->whereMonth('tanggal','12')->get();
        $pengeluaran      = Kas::whereYear('tanggal','2021')->whereMonth('tanggal','12')->get();
        $kas              = $order->sum('harga_jual');

        return view('home',compact('cod','transfer','order','order_cod','order_transfer','kas','today_order','today_transfer','today_cod'));
    }
    public function jx()
    {
        return view('jx');
    }
}