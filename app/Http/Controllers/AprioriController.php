<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProcessLog;
use App\Confidence;
use App\Order;
use App\Transaksi;
use App\Itemset1;
use App\Itemset2;
use App\Itemset3;
use DB;

class AprioriController extends Controller
{
	public function index()
	{
		return view('apriori');
	}
	public function tambahProcessLog($min_support, $min_confidence, $start, $end) {
		$insert = new ProcessLog;
		$insert->start_date	 	= $start;
		$insert->end_date		= $end;
		$insert->min_support	= $min_support;
		$insert->min_confidence = $min_confidence;
		$insert->save();
	}

	public function getLastIdProcessLog() {
		$last = ProcessLog::orderBy('id','DESC')->first();
		return $last;
	}
    public function get_variasi_itemset3($array_itemset3, $item1, $item2, $item3, $item4) {
		$return=array();

		$return1=array();

		if( !in_array(strtoupper($item1), array_map('strtoupper', $return1))) {
			$return1[]=$item1;
		}

		if( !in_array(strtoupper($item2), array_map('strtoupper', $return1))) {
			$return1[]=$item2;
		}

		if( !in_array(strtoupper($item3), array_map('strtoupper', $return1))) {
			$return1[]=$item3;
		}

		$return2=array();

		if( !in_array(strtoupper($item1), array_map('strtoupper', $return2))) {
			$return2[]=$item1;
		}

		if( !in_array(strtoupper($item2), array_map('strtoupper', $return2))) {
			$return2[]=$item2;
		}

		if( !in_array(strtoupper($item4), array_map('strtoupper', $return2))) {
			$return2[]=$item4;
		}

		$return3=array();

		if( !in_array(strtoupper($item1), array_map('strtoupper', $return3))) {
			$return3[]=$item1;
		}

		if( !in_array(strtoupper($item3), array_map('strtoupper', $return3))) {
			$return3[]=$item3;
		}

		if( !in_array(strtoupper($item4), array_map('strtoupper', $return3))) {
			$return3[]=$item4;
		}

		$return4=array();

		if( !in_array(strtoupper($item2), array_map('strtoupper', $return4))) {
			$return4[]=$item2;
		}

		if( !in_array(strtoupper($item3), array_map('strtoupper', $return4))) {
			$return4[]=$item3;
		}

		if( !in_array(strtoupper($item4), array_map('strtoupper', $return4))) {
			$return4[]=$item4;
		}

		if(count($return1)==3) {
			if( !$this->is_exist_variasi_on_itemset3($return, $return1)) {
				if( !$this->is_exist_variasi_on_itemset3($array_itemset3, $return1)) {
					$return[]=$return1;
				}
			}
		}

		if(count($return2)==3) {
			if( !$this->is_exist_variasi_on_itemset3($return, $return2)) {
				if( !$this->is_exist_variasi_on_itemset3($array_itemset3, $return2)) {
					$return[]=$return2;
				}
			}
		}

		if(count($return3)==3) {
			if( !$this->is_exist_variasi_on_itemset3($return, $return3)) {
				if( !$this->is_exist_variasi_on_itemset3($array_itemset3, $return3)) {
					$return[]=$return3;
				}
			}
		}

		if(count($return4)==3) {
			if( !$this->is_exist_variasi_on_itemset3($return, $return4)) {
				if( !$this->is_exist_variasi_on_itemset3($array_itemset3, $return4)) {
					$return[]=$return4;
				}
			}
		}

		return $return;
	}

	public function is_exist_variasi_on_itemset3($array, $tiga_variasi){
		$return = false;
		
		foreach ($array as $key => $value) {
			$jml=0;
			foreach ($value as $key1 => $val1) {
				foreach ($tiga_variasi as $key2 => $val2) {
					if(strtoupper($val1) == strtoupper($val2)){
						$jml++;
					}
				}
			}
			if($jml==3){
				$return=true;
				break;
			}
		}
		
		return $return;
	}

	public function is_exist_variasi_itemset($array_item1, $array_item2, $item1, $item2) {
		
		$bool1=array_keys(array_map('strtoupper', $array_item1), strtoupper($item1));
		$bool2=array_keys(array_map('strtoupper', $array_item2), strtoupper($item2));
		$bool3=array_keys(array_map('strtoupper', $array_item2), strtoupper($item1));
		$bool4=array_keys(array_map('strtoupper', $array_item1), strtoupper($item2));

		foreach ($bool1 as $key=> $value) {
			$aa=array_search($value, $bool2);

			if(is_numeric($aa)) {
				return true;
			}
		}

		foreach ($bool3 as $key=> $value) {
			$aa=array_search($value, $bool4);

			if(is_numeric($aa)) {
				return true;
			}
		}

		return false;
	}

	public function jumlah_itemset1($transaksi_list, $produk) {
		$count = 0;
		foreach ($transaksi_list as $key=> $data) {
			$items=",".strtoupper($data['produk']);
			$item_cocok=",".strtoupper($produk).",";
			//Fungsi strpos() adalah fungsi bawaan PHP yang bisa digunakan untuk mencari posisi sebuah karakter atau sebuah string di dalam string lainnya
			$pos=strpos($items, $item_cocok);

			if($pos !==false) {
				//was found at position $pos
				$count++;
			}
		}

		return $count;
	}

	public function jumlah_itemset2($transaksi_list, $variasi1, $variasi2) {
		$count=0;

		foreach ($transaksi_list as $key=> $data) {
			$items=",".strtoupper($data['produk']);
			$item_variasi1=",".strtoupper($variasi1).",";
			$item_variasi2=",".strtoupper($variasi2).",";

			$pos1=strpos($items, $item_variasi1);
			$pos2=strpos($items, $item_variasi2);

			if($pos1 !== false && $pos2 !== false) {
				//was found at position $pos
				$count++;
			}
		}

		return $count;
	}

	public function jumlah_itemset3($transaksi_list, $variasi1, $variasi2, $variasi3) {
		$count=0;

		foreach ($transaksi_list as $key=> $data) {
			$items=",".strtoupper($data['produk']);
			$item_variasi1=",".strtoupper($variasi1).",";
			$item_variasi2=",".strtoupper($variasi2).",";
			$item_variasi3=",".strtoupper($variasi3).",";

			$pos1=strpos($items, $item_variasi1);
			$pos2=strpos($items, $item_variasi2);
			$pos3=strpos($items, $item_variasi3);

			if($pos1 !==false && $pos2 !==false && $pos3 !==false) {
				//was found at position $pos
				$count++;
			}
		}

		return $count;
	}
	public function hitung_confidence($supp_xuy, $min_support, $min_confidence, $atribut1, $atribut2, $atribut3, $id_process, $dataTransaksi, $jumlah_transaksi) {

		//hitung nilai support $nilai_support_x seperti di itemset2
		$jml_itemset2 = $this->jumlah_itemset2($dataTransaksi, $atribut1, $atribut2);
		$nilai_support_x = ($jml_itemset2/$jumlah_transaksi) * 100;

		$kombinasi1 = $atribut1." , ".$atribut2;
		$kombinasi2 = $atribut3;
		$supp_x = $nilai_support_x; //$row1_['support'];
		$conf = ($supp_xuy/$supp_x)*100;
		//lolos seleksi min confidence itemset3
		$lolos = ($conf >= $min_confidence)? 1: 0;

		//hitung korelasi lift
		$jumlah_kemunculanAB = $this->jumlah_itemset3($dataTransaksi, $atribut1, $atribut2, $atribut3);
		$PAUB = $jumlah_kemunculanAB/$jumlah_transaksi;

		$jumlah_kemunculanA = $this->jumlah_itemset2($dataTransaksi, $atribut1, $atribut2);
		$jumlah_kemunculanB = $this->jumlah_itemset1($dataTransaksi, $atribut3);

		//$nilai_uji_lift = $PAUB / $jumlah_kemunculanA * $jumlah_kemunculanB;
		$nilai_uji_lift = $PAUB / (($jumlah_kemunculanA/$jumlah_transaksi) * ($jumlah_kemunculanB/$jumlah_transaksi));
		$korelasi_rule = ($nilai_uji_lift < 1)?"korelasi negatif": "korelasi positif";

		if($nilai_uji_lift == 1) {
			$korelasi_rule = "tidak ada korelasi";
		}

		//masukkan ke table confidence
		$insert = new Confidence;
		$insert->kombinasi1		= $kombinasi1;
		$insert->kombinasi2		= $kombinasi2;
		$insert->support_xUy	= $supp_xuy;
		$insert->support_x		= $supp_x;
		$insert->confidence		= $conf;
		$insert->lolos			= $lolos;
		$insert->min_support	= $min_support;
		$insert->min_confidence	= $min_confidence;
		$insert->nilai_uji_lift	= $nilai_uji_lift;
		$insert->korelasi_rule	= $korelasi_rule;
		$insert->id_process		= $id_process;
		$insert->jumlah_a		= $jumlah_kemunculanA;
		$insert->jumlah_b		= $jumlah_kemunculanB;
		$insert->jumlah_ab		= $jumlah_kemunculanAB;
		$insert->px				= ($jumlah_kemunculanA/$jumlah_transaksi);
		$insert->py				= ($jumlah_kemunculanB/$jumlah_transaksi);
		$insert->pxuy			= $PAUB;
		$insert->from_itemset	= 3;
		$insert->save();

	}
	public function hitung_confidence1($supp_xuy, $min_support, $min_confidence, $atribut1, $atribut2, $atribut3, $id_process, $dataTransaksi, $jumlah_transaksi) {

		//hitung nilai support seperti itemset1
		$jml_itemset1 		= $this->jumlah_itemset1($dataTransaksi, $atribut1);
		$nilai_support_x	= ($jml_itemset1/$jumlah_transaksi) * 100;

		$kombinasi1 		= $atribut1;
		$kombinasi2 		= $atribut2." , ".$atribut3;
		$supp_x 			= $nilai_support_x; //$row4_['support'];
		$conf 				= ($supp_xuy/$supp_x)*100;
		//lolos seleksi min confidence itemset3
		$lolos 				= ($conf >= $min_confidence)? 1: 0;

		//hitung korelasi lift
		$jumlah_kemunculanAB	= $this->jumlah_itemset3($dataTransaksi, $atribut1, $atribut2, $atribut3);
		$PAUB					= $jumlah_kemunculanAB/$jumlah_transaksi;

		$jumlah_kemunculanA	= $this->jumlah_itemset1($dataTransaksi, $atribut1);
		$jumlah_kemunculanB	= $this->jumlah_itemset2($dataTransaksi, $atribut2, $atribut3);

		$nilai_uji_lift = $PAUB / (($jumlah_kemunculanA/$jumlah_transaksi) * ($jumlah_kemunculanB/$jumlah_transaksi));
		$korelasi_rule 	= ($nilai_uji_lift < 1)?"korelasi negatif": "korelasi positif";

		if($nilai_uji_lift == 1) {
			$korelasi_rule = "tidak ada korelasi";
		}


		//masukkan ke table confidence
		$insert = new Confidence;
		$insert->kombinasi1 		= $kombinasi1;
		$insert->kombinasi2 		= $kombinasi2;
		$insert->support_xUy 		= $supp_xuy;
		$insert->support_x 			= $supp_x;
		$insert->confidence 		= $conf;
		$insert->lolos 				= $lolos;
		$insert->min_support 		= $min_support;
		$insert->min_confidence 	= $min_confidence;
		$insert->nilai_uji_lift 	= $nilai_uji_lift;
		$insert->korelasi_rule 		= $korelasi_rule;
		$insert->id_process 		= $id_process;
		$insert->jumlah_a 			= $jumlah_kemunculanA;
		$insert->jumlah_b 			= $jumlah_kemunculanB;
		$insert->jumlah_ab 			= $jumlah_kemunculanAB;
		$insert->px 				= ($jumlah_kemunculanA/$jumlah_transaksi);
		$insert->py 				= ($jumlah_kemunculanB/$jumlah_transaksi);
		$insert->pxuy 				= $PAUB;
		$insert->from_itemset 		= 3;
		$insert->save();
		
	}

	public function hitung_confidence2($supp_xuy, $min_support, $min_confidence,$atribut1, $atribut2, $id_process, $dataTransaksi, $jumlah_transaksi) {
		//hitung nilai support seperti itemset1
		$jml_itemset1		= $this->jumlah_itemset1($dataTransaksi, $atribut1);
		$nilai_support_x	= ($jml_itemset1/$jumlah_transaksi) * 100;

		$kombinasi1			= $atribut1;
		$kombinasi2			= $atribut2;
		$supp_x				= $nilai_support_x; //$row1_['support'];
		$conf				= ($supp_xuy/$supp_x)*100;
		//lolos seleksi min confidence itemset3
		$lolos 				= ($conf >= $min_confidence)? 1: 0;

		//hitung korelasi lift
		$jumlah_kemunculanAB = $this->jumlah_itemset2($dataTransaksi, $atribut1, $atribut2);
		$PAUB				 = $jumlah_kemunculanAB/$jumlah_transaksi;

		$jumlah_kemunculanA	= $this->jumlah_itemset1($dataTransaksi, $atribut1);
		$jumlah_kemunculanB	= $this->jumlah_itemset1($dataTransaksi, $atribut2);

		$nilai_uji_lift	= $PAUB / (($jumlah_kemunculanA/$jumlah_transaksi) * ($jumlah_kemunculanB/$jumlah_transaksi));
		$korelasi_rule	= ($nilai_uji_lift < 1) ? "korelasi negatif": "korelasi positif";

		if($nilai_uji_lift == 1) {
			$korelasi_rule = "tidak ada korelasi";
		}

        //masukkan ke table confidence
        $insert = new Confidence;
		$insert->kombinasi1 		= $kombinasi1;
		$insert->kombinasi2 		= $kombinasi2;
		$insert->support_xUy 		= $supp_xuy;
		$insert->support_x 			= $supp_x;
		$insert->confidence 		= $conf;
		$insert->lolos 				= $lolos;
		$insert->min_support 		= $min_support;
		$insert->min_confidence 	= $min_confidence;
		$insert->nilai_uji_lift 	= $nilai_uji_lift;
		$insert->korelasi_rule 		= $korelasi_rule;
		$insert->id_process 		= $id_process;
		$insert->jumlah_a 			= $jumlah_kemunculanA;
		$insert->jumlah_b 			= $jumlah_kemunculanB;
		$insert->jumlah_ab 			= $jumlah_kemunculanAB;
		$insert->px 				= ($jumlah_kemunculanA/$jumlah_transaksi);
		$insert->py 				= ($jumlah_kemunculanB/$jumlah_transaksi);
		$insert->pxuy 				= $PAUB;
        $insert->from_itemset 		= 2;
        $insert->save();

	}

	public function miningProcess($min_support, $min_confidence, $start, $end) {
		
		//Tambah proses baru ke database
		$this->tambahProcessLog($min_support,$min_confidence,$start,$end);

		//Ambil data dari data transaksi
		$sql_trans = Transaksi::whereBetween('transaction_date',[$start,$end]);
		$result_trans = $sql_trans->get();

		//Hitung jumlah data transaksi
		$jumlah_transaksi = count($result_trans);

		//inisialisasi
		$dataTransaksi = $myrow = $item_list = array();

		//hitung minimum suport relative
		$min_support_relative = ($min_support/$jumlah_transaksi)*100;

		//looping input data ke $dataTransaksi
		$x=0;
		foreach($result_trans as $myrow) {

			//input tanggal transaksi
			$dataTransaksi[$x]['tanggal'] = $myrow->tanggal;
			
			//mencegah ada jarak spasi di antara item
			//masalah disini
			$item_produk = $myrow->produk.",";
			$item_produk = str_replace(" ,", ",", $item_produk);
			$item_produk = str_replace("  ,", ",", $item_produk);
			$item_produk = str_replace("   ,", ",", $item_produk);
			$item_produk = str_replace("    ,", ",", $item_produk);
			$item_produk = str_replace(", ", ",", $item_produk);
			$item_produk = str_replace(",  ", ",", $item_produk);
			$item_produk = str_replace(",   ", ",", $item_produk);
			$item_produk = str_replace(",    ", ",", $item_produk);

			//input data produk yang telah bersih
			$dataTransaksi[$x]['produk'] = $item_produk;

			//explode = memisahkan atau memecah-mecahkan suatu string berdasarkan tanda pemisah
			$produk = explode(",", $myrow['produk']);
			// dd($produk);

			//cek duplikasi item produk dan mengubah ke huruf kapital
			//input data ke $item_list
			foreach ($produk as $key=>$value_produk) {
				//Fungsi strtoupper ini adalah suatu perintah yang ada pada PHP untuk membuat suatu string menjadi huruf kapital
				if( !in_array(strtoupper($value_produk), array_map('strtoupper', $item_list))) {
					if( !empty($value_produk)) {
						$item_list[]=$value_produk;
					}
				}
			}
			$x++;
		}

		//inisialisasi id process
		$id_Yuhuu = $this->getLastIdProcessLog();
		$id_process = $id_Yuhuu->id;
		
		//build itemset 1
		$itemset1 = $jumlahItemset1 = $supportItemset1 = $valueIn = array();
		$x = 1;

		foreach ($item_list as $key => $item) {
			//menghitung jumlah masing masing item dalam semua transaksi
			$jumlah = $this->jumlah_itemset1($dataTransaksi, $item);
			$support = ($jumlah/$jumlah_transaksi) * 100;	
			if ($support >= $min_support_relative){
				$lolos = 1;
			}else{
				$lolos = 0;
			}
			// $lolos=($support>=$min_support_relative)?1: 0;
			$valueIn[] = "('$item','$jumlah','$support','$lolos','$id_process')";

			if($lolos == 1) {
				$itemset1[] 		= $item; //item yg lolos itemset1
				$jumlahItemset1[] 	= $jumlah;
				$supportItemset1[] 	= $support;
			}
			$x++;
		}
		
		//insert into itemset1 one query with many value
		if ($valueIn){
			$value_insert 		 = implode(",", $valueIn);
			$sql_insert_itemset1 = DB::statement("INSERT INTO itemset1 (atribut, jumlah, support, lolos, id_process) VALUES ".$value_insert);
		}

		//build itemset2
		$NilaiAtribut1 = $NilaiAtribut2 = array();
		$itemset2_var1 = $itemset2_var2 = $jumlahItemset2 = $supportItemset2 = array();
		$valueIn_itemset2 = array();
		$no = 1;
		$a = 0;

		while ($a < count($itemset1)) {
			$b=0;
			while ($b < count($itemset1)) {
				$variance1 = $itemset1[$a];
				$variance2 = $itemset1[$b];
				if ( !empty($variance1) && !empty($variance2)) {
					if ($variance1 !=$variance2) {
						if( !$this->is_exist_variasi_itemset($NilaiAtribut1, $NilaiAtribut2, $variance1, $variance2)) {
							//$jml_itemset2 = get_count_itemset2($db_object, $variance1, $variance2, $start_date, $end_date);
							$jml_itemset2 = $this->jumlah_itemset2($dataTransaksi, $variance1, $variance2);
							$NilaiAtribut1[] = $variance1;
							$NilaiAtribut2[] = $variance2;
							$support2 = ($jml_itemset2/$jumlah_transaksi) * 100;
							$lolos = ($support2 >= $min_support_relative) ? 1: 0;
							$valueIn_itemset2[] = "('$variance1','$variance2','$jml_itemset2','$support2','$lolos','$id_process')";
							if($lolos) {
								$itemset2_var1[]	= $variance1;
								$itemset2_var2[]	= $variance2;
								$jumlahItemset2[]	= $jml_itemset2;
								$supportItemset2[]	= $support2;
							}
							$no++;
						}
					}
				}
				$b++;
			}
			$a++;
		}

		//insert into itemset2 one query with many value
		if ($valueIn_itemset2){
			$value_insert_itemset2 = implode(",", $valueIn_itemset2);		
			$sql_insert_itemset2 = DB::statement("INSERT INTO itemset2 (atribut1, atribut2, jumlah, support, lolos, id_process) VALUES ".$value_insert_itemset2);
		}
		
		//build itemset3
		$a=0;
		$tigaVariasiItem = $valueIn_itemset3 = array();
		$itemset3_var1 = $itemset3_var2 = $itemset3_var3 = $jumlahItemset3 = $supportItemset3 = array();
		$no = 1;

		while ($a < count($itemset2_var1)) {
			$b=0;

			while ($b < count($itemset2_var1)) {
				if($a != $b) {
					$itemset1a = $itemset2_var1[$a];
					$itemset1b = $itemset2_var1[$b];
					$itemset2a = $itemset2_var2[$a];
					$itemset2b = $itemset2_var2[$b];

					if ( !empty($itemset1a) && !empty($itemset1b)&& !empty($itemset2a) && !empty($itemset2b)) {
						$temp_array=$this->get_variasi_itemset3($tigaVariasiItem, $itemset1a, $itemset1b, $itemset2a, $itemset2b);
						if(count($temp_array) > 0) {
							//variasi-variasi itemset isi ke array
							$tigaVariasiItem = array_merge($tigaVariasiItem, $temp_array);

							foreach ($temp_array as $idx=> $val_nilai) {
								$itemset1 = $itemset2 = $itemset3 = "";
								$aaa=0;
								foreach ($val_nilai as $idx1 => $v_nilai) {
									if($aaa == 0) {
										$itemset1 = $v_nilai;
									}
									if($aaa == 1) {
										$itemset2 = $v_nilai;
									}
									if($aaa == 2) {
										$itemset3 = $v_nilai;
									}
									$aaa++;
								}

								//jumlah item set3 dan menghitung supportnya
								$jml_itemset3 = $this->jumlah_itemset3($dataTransaksi, $itemset1, $itemset2, $itemset3);
								$support3 = ($jml_itemset3/$jumlah_transaksi) * 100;
								$lolos = ($support3 >= $min_support_relative) ? 1:0;

								$valueIn_itemset3[]="('$itemset1','$itemset2','$itemset3','$jml_itemset3','$support3','$lolos','$id_process')";

								if($lolos) {
									$itemset3_var1[]	= $itemset1;
									$itemset3_var2[]	= $itemset2;
									$itemset3_var3[]	= $itemset3;
									$jumlahItemset3[]	= $jml_itemset3;
									$supportItemset3[]	= $support3;
								}
							}
						}
					}
				}
				$b++;
			}

			$a++;
		}

		//insert into itemset3 one query with many value
		if($valueIn_itemset3){
			$value_insert_itemset3	=	implode(",", $valueIn_itemset3);
			$sql_insert_itemset3 = DB::statement("INSERT INTO itemset3 (atribut1, atribut2, atribut3, jumlah, support, lolos, id_process) VALUES ".$value_insert_itemset3);
		}

		//hitung confidence
		$confidence_from_itemset = 0;
		//dari itemset 3 jika tidak ada yg lolos ambil dari itemset 2 jika tiak ada gagal mendapatkan confidence
		$res_3 = Itemset3::where('lolos',1)->where('id_process',$id_process)->get();
		$jumlah_itemset3_lolos = $res_3->count();

		if($jumlah_itemset3_lolos > 0) {
			$confidence_from_itemset = 3;

			// while($row_3=$this->db->mysqli_fetch_array($res_3)) {
			foreach($res_3 as $row_3) {
			
				$atribut1 = $row_3['atribut1'];
				$atribut2 = $row_3['atribut2'];
				$atribut3 = $row_3['atribut3'];
				$supp_xuy = $row_3['support'];

				//1,2 => 3
				$this->hitung_confidence($supp_xuy, $min_support, $min_confidence,$atribut1, $atribut2, $atribut3, $id_process, $dataTransaksi, $jumlah_transaksi);

				//2,3 => 1
				$this->hitung_confidence($supp_xuy, $min_support, $min_confidence,$atribut2, $atribut3, $atribut1, $id_process, $dataTransaksi, $jumlah_transaksi);

				//3,1 => 2
				$this->hitung_confidence($supp_xuy, $min_support, $min_confidence,$atribut3, $atribut1, $atribut2, $id_process, $dataTransaksi, $jumlah_transaksi);


				//1 => 3,2
				$this->hitung_confidence1($supp_xuy, $min_support, $min_confidence,$atribut1, $atribut3, $atribut2, $id_process, $dataTransaksi, $jumlah_transaksi);

				//2 => 1,3
				$this->hitung_confidence1($supp_xuy, $min_support, $min_confidence,$atribut2, $atribut1, $atribut3, $id_process, $dataTransaksi, $jumlah_transaksi);

				//3 => 2,1
				$this->hitung_confidence1($supp_xuy, $min_support, $min_confidence,$atribut3, $atribut2, $atribut1, $id_process, $dataTransaksi, $jumlah_transaksi);
			}
		}

		//dari itemset 2
		$res_2 = Itemset2::where('lolos',1)->where('id_process',$id_process)->get();
		$jumlah_itemset2_lolos = $res_2->count();

		if($jumlah_itemset2_lolos > 0) {
			$confidence_from_itemset = 2;

			//while($row_2=$this->db->mysqli_fetch_array($res_2)) {
			foreach($res_2 as $row_2) {
			
				$atribut1 = $row_2['atribut1'];
				$atribut2 = $row_2['atribut2'];
				$supp_xuy = $row_2['support'];

				//1 => 2
				$this->hitung_confidence2($supp_xuy, $min_support, $min_confidence, $atribut1, $atribut2, $id_process, $dataTransaksi, $jumlah_transaksi);

				//2 => 1
				$this->hitung_confidence2($supp_xuy, $min_support, $min_confidence, $atribut2, $atribut1, $id_process, $dataTransaksi, $jumlah_transaksi);
			}
		}
		if($confidence_from_itemset == 0) {
			return false;
		}else{
			return true;
		}		

	}
	public function prosesApriori(Request $request)
    {
        $awal 			= microtime(true);
        $min_support 	= $request->min_support;
        $min_confidence	= $request->min_confidence;

		$tgl 	= explode(" - ", $request->tanggal);
		$start 	= date('Y-m-d',strtotime($tgl[0]));
        $end 	= date('Y-m-d',strtotime($tgl[1]));

        
       
        $mining 	= $this->miningProcess($min_support,$min_confidence,$start,$end);
        $lastId		= $this->getLastIdProcessLog();
        $last 		= $lastId->id;
        $akhir 		= microtime(true);
        $hasil = $akhir - $awal;

        if ($mining) {
            // $this->session->set_flashdata('success', 'Mining Berhasil '.$hasil.'detik');
            return redirect('hasil/'.$last)->with('error','Maaf terjadi kesalahan');
        } else {
        	return redirect()->back()->with('error','Maaf terjadi kesalahan');
        }
    }
    public function hasil($id)
    {

    	$ConfidenceItemset3 	= Confidence::where('id_process',$id)->join('process_log','confidence.id_process','=','process_log.id')->select('confidence.*','process_log.start_date', 'process_log.end_date')->where('from_itemset','3')->get();
    	$ConfidenceItemset2 	= Confidence::where('id_process',$id)->join('process_log','confidence.id_process','=','process_log.id')->select('confidence.*','process_log.start_date', 'process_log.end_date')->where('from_itemset','2')->get();
        $RuleID 				= ProcessLog::find($id);
        $ItemSet1 				= Itemset1::where('id_process',$id)->orderBy('support','DESC')->get();
        $ItemSet2 				= Itemset2::where('id_process',$id)->orderBy('support','DESC')->get();
        $ItemSet3 				= Itemset3::where('id_process',$id)->orderBy('support','DESC')->get();

        $RuleID = ProcessLog::find($id);

    	return view('hasil',compact('RuleID','ItemSet1','ItemSet2','ItemSet3','ConfidenceItemset2','ConfidenceItemset3'));
    }
    public function process_log()
    {
    	$process_log 	= ProcessLog::all();
    	return view('process_log',compact('process_log'));
    }
}
