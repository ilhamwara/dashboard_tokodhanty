<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Produk;
use DB;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class MovingAverageController extends Controller
{
	public function weeksBetweenTwoDates($start, $end)
	{
	    $weeks = [];

	    while ($start->weekOfYear !== $end->weekOfYear) {
	        $weeks[] = [
	            'from' => $start->startOfWeek()->format('Y-m-d'),
	            'to' => $start->endOfWeek()->format('Y-m-d')
	        ];

	        $start->addWeek(1);
	    }

	    return $weeks;
	}
    public function index()
    {
    	$pro = Produk::all();

		$minggu = $this->weeksBetweenTwoDates(Carbon::now()->month(request()->periode)->subDays(30), Carbon::now()->month(request()->periode));
		// dd();

    	return view('moving.index',compact('pro','minggu'));
    }
}
