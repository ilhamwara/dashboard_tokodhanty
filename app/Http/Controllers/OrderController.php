<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\OrderDataTable;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\Order as OrderImport;
use App\Order;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(OrderDataTable $dataTable)
    {
        // dd(request()->all());
        if (empty(request()->date)) {
            return $dataTable->with(['status' => request()->status,'tanggal_dari' => 'kosong'])->render('order.index');
        }else{
            $date = explode(' - ', request()->date);
            $tanggal_dari   = $date[0];
            $tanggal_sampai = $date[1];
            $order = Order::whereBetween('tanggal',[$tanggal_dari,$tanggal_sampai])->get();

            return $dataTable->with(['status' => request()->status,'tanggal_dari' => $tanggal_dari, 'tanggal_sampai' => $tanggal_sampai,'type' => request()->type])->render('order.index',compact('order','tanggal_dari','tanggal_sampai'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $import  = Excel::import(new OrderImport(), $request->file);
            
            return redirect()->back()->with('alert-success', 'Succes Import Data!');
        } catch (Exception $e) {
            return redirect()->back()->with('alert-danger', 'Gagal');
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function approve($id)
    {
        $order = Order::findOrFail($id);
        $order->status = 'selesai';
        $order->save();

        return redirect()->back()->with('alert-success','Berhasil update data');
    }
    public function retur($id)
    {
        $order = Order::findOrFail($id);
        $order->status = 'retur';
        $order->save();

        return redirect()->back()->with('alert-success','Berhasil update data');
    }
}
