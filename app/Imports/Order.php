<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

use App\Order as ModelOrder;
use App\User;
use App\Produk;

class Order implements ToModel,WithChunkReading,WithStartRow,WithColumnFormatting
{
    public function model(array $row)
    {
    	// $user      = User::where('username',$row[0])->first();
    	$produk    = Produk::where('nama','LIKE','%'.$row[11].'%')->first();
        $order 	   = new ModelOrder;
        $order->user_id         = 1;
        $order->nomer_pesanan   = time();
        $order->produk_id       = $produk->id;
        $order->tanggal         = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[0]);
        $order->resi            = time();
        $order->nama            = $row[4];
        $order->hp              = $row[6];
        $order->alamat          = $row[5];
        $order->kurir           = $row[14];
        $order->spesifikasi     = '';
        $order->type            = $row[3];
        $order->jumlah          = $row[13];
        $order->harga_jual      = str_replace('Rp','',(empty($row[8]) ? $row[9] : $row[8]));
        $order->diskon          = 0;
        $order->ongkir          = 0;
        $order->status          = 'TERKIRIM';
        $order->save();
        return $order;
    }
    public function startRow(): int {
         return 2;
    }
    public function chunkSize(): int
    {
        return 1000;
    }
    public function columnFormats(): array
    {
        return [
            'K' => '0',
            'J' => '0',
        ];
    }
}
