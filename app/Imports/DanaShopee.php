<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\Kas;

class DanaShopee implements ToModel,WithChunkReading,WithStartRow
{
    public function model(array $row)
    {
    	$kas = new Kas;
    	$kas->tanggal 		= $row[0];
    	$kas->keterangan 	= 'Pelepasan Dana dari Nomer Pesanan : '.$row[1];
    	$kas->pemasukan 	= $row[2];
    	$kas->pengeluaran 	= 0;
    	$kas->save();

        return $kas;
    }
    public function startRow(): int {
         return 2;
    }
    public function chunkSize(): int
    {
        return 1000;
    }
}
