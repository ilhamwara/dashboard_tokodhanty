<?php

namespace App\Imports;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\Kas as ModelKas;

class Kas implements ToModel,WithChunkReading,WithStartRow
{
    public function model(array $row)
    {
    	$kas = new ModelKas;
    	$kas->tanggal 		= \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[0]);
    	$kas->keterangan 	= $row[1];
    	$kas->pemasukan 	= $row[2];
    	$kas->pengeluaran 	= $row[3];
    	$kas->save();

        return $kas;
    }
    public function startRow(): int {
         return 2;
    }
    public function chunkSize(): int
    {
        return 1000;
    }
}
