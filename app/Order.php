<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
    	'user_id',
    	'nomer_pesanan',
    	'produk_id',
    	'tanggal',
    	'resi',
    	'nama',
    	'hp',
    	'alamat',
    	'kurir',
    	'spesifikasi',
    	'type',
    	'harga_jual',
    	'diskon',
		'ongkir',
		'status',
    ];

    public function produk()
    {
        return $this->belongsTo('\App\Produk','produk_id','id');
    }
    public function user()
    {
        return $this->belongsTo('\App\User','user_id','id');
    }
}
